This YAWIK module pushes job opening via Vendor API to XING.

Requirements
------------

- A running YAWIK
- Xing Account for publishing jobs

more infos:

https://www.helpify.de/xing-posting-api-en/2937/how-can-i-as-a-developer-use-the-xing-posting-api

Installation
------------

Checkout this repository into you modules directory of your YAWIK installation and enable the module by:

<pre>
cp modules/YawikXingVendorApi/config/YawikXingVendorApi.module.php.dist consfig/autoload/YawikXingVendorApi.module.php
</pre>

Or install the module using composer

<pre>
composer require cross-solution/yawik-company-registration
</pre>

Licence 
-------

MIT

https://github.com/cross-solution/YawikXingVendorApi/blob/develop/LICENSE

